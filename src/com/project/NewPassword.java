/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;


public class NewPassword extends JFrame implements ActionListener{
    private JPasswordField newPassField;
    private JButton Confirm;
    public NewPassword() {
         setBounds(500, 200, 518, 442);
         setResizable(false);
         setLayout(null);
         setDefaultCloseOperation(EXIT_ON_CLOSE);
         
         JLabel PassLabel = new JLabel("New Password:");
         PassLabel.setFont(new Font("Arial", Font.PLAIN, 14));
         PassLabel.setBounds(50,140,100,10);
         add(PassLabel);
         
          newPassField = new JPasswordField();
          newPassField.setFont(new Font("Arial", Font.PLAIN, 15));
          newPassField.setBounds(160, 130, 200, 30);
          add(newPassField);
          
          Confirm = new JButton("Confirm");
          Confirm.addActionListener(this);
          Confirm.setBackground(new Color(211, 211, 211));
          Confirm.setForeground(Color.BLACK);
          Confirm.setBounds(170, 180, 100, 30);
          add(Confirm);
          
    }
    
    

    public void actionPerformed(ActionEvent e) {
        try {
            String email = "luqmanwaheed540@gmail.com"; 
        if(e.getSource() == Confirm && newPassField.getText() != "") {
            conn con = new conn();
            String sql = "UPDATE user SET Password=? WHERE Email=?";
            PreparedStatement st = con.c.prepareStatement(sql);
            st.setString(1, newPassField.getText());
            st.setString(2, email);
            int i = st.executeUpdate();
            
            if(i > 0) {
                JOptionPane.showMessageDialog(null, "Password Updated Successfully...!.");
                this.setVisible(false);
            }
        }
        } catch(Exception e2) {
            e2.printStackTrace();
        }
    }
    public static void main(String[] args) {
        new NewPassword().setVisible(true);
    }
    
}
