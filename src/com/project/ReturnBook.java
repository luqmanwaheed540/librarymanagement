package com.project;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import com.toedter.calendar.JDateChooser;
import java.awt.event.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ReturnBook extends JFrame implements ActionListener{

    private JPanel contentPane;
    private JTextField BookIdField, StudentIdField, BookTitleField, StudentNameField, IssuedDateField, ReturnedDateField, FineField;
    private JButton SearchBtn, ReturnBtn, BackBtn;
    //private JDateChooser dateChooser;

    public static void main(String[] args) {
	new ReturnBook().setVisible(true);
    }

    public int AddFine(String Id) {
         try {
             double fineadded=Double.parseDouble(FineField.getText());
             double currentfine=0.0;
             double Fine;
            conn con = new conn();
            String finequery = "SELECT * from user where Id=?";
            PreparedStatement st = con.c.prepareStatement(finequery);
            st.setString(1, Id);
            ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    currentfine=Double.parseDouble(rs.getString("Fine"));
                    Fine=currentfine+fineadded;
                    System.out.println("Fine: " + Fine);
                    String Finetxt=Double.toString(Fine);
                    String insertFine = "UPDATE `user` SET `Fine`=? WHERE Id=?";
                    PreparedStatement st2 = con.c.prepareStatement(insertFine);
                    st2.setString(1, Finetxt);
                    st2.setString(2, Id);
                    int i=st2.executeUpdate();
                    System.out.println("Fine: " + Fine);
                    if(i>0) {
                        return 1;
                    }
                    System.out.println(Finetxt);
                }
        } catch (SQLException e) {
            System.out.println("Error");
            JOptionPane.showMessageDialog(null, e);
            e.printStackTrace();
	}
         return 0;
    }
    
    String CurrentDate() {
        DateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy");
            java.util.Date date = new java.util.Date();
            String currentDate = dateformat.format(date);
            return currentDate;
    }
    
    public void delete() {
        try {
            conn con = new conn();
            String sql = "DELETE from issued_books where Book_Id=?";
            PreparedStatement st = con.c.prepareStatement(sql);
            st.setString(1, BookIdField.getText());
            int i = st.executeUpdate();
            if (i > 0)
                JOptionPane.showMessageDialog(null, "Book Returned");
            else
                JOptionPane.showMessageDialog(null, "Error in Deleting");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            e.printStackTrace();
	}
    }

    
    ReturnBook() {
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
        //setBounds(450, 300, 617, 363);
	contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setLayout(null);
        contentPane.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 5, true), "Return Book",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        contentPane.setBounds(70,70,550,400);
        add(contentPane);

	JLabel lblNewLabel = new JLabel("Book_Id");
	lblNewLabel.setForeground(Color.WHITE);
	lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblNewLabel.setBounds(30, 50, 87, 24);
	contentPane.add(lblNewLabel);

	JLabel lblStudentid = new JLabel("Student_Id");
	lblStudentid.setForeground(Color.WHITE);
	lblStudentid.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblStudentid.setBounds(220, 50, 87, 24);
	contentPane.add(lblStudentid);

	JLabel lblBook = new JLabel("Book Title");
	lblBook.setForeground(Color.WHITE);
	lblBook.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblBook.setBounds(30, 100, 71, 24);
	contentPane.add(lblBook);

	JLabel lblName = new JLabel("Student Name");
	lblName.setForeground(Color.WHITE);
	lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblName.setBounds(30, 148, 120, 24);
	contentPane.add(lblName);

	JLabel lblDateOfIssue = new JLabel("Date of Issue");
	lblDateOfIssue.setForeground(Color.WHITE);
	lblDateOfIssue.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblDateOfIssue.setBounds(30, 190, 105, 29);
	contentPane.add(lblDateOfIssue);

	JLabel lblDateOfReturn = new JLabel("Date of Return");
	lblDateOfReturn.setForeground(Color.WHITE);
	lblDateOfReturn.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblDateOfReturn.setBounds(30, 235, 118, 29);
	contentPane.add(lblDateOfReturn);
        
        JLabel Fine = new JLabel("Fine");
	Fine.setForeground(Color.WHITE);
	Fine.setFont(new Font("Tahoma", Font.BOLD, 14));
	Fine.setBounds(30, 280, 118, 29);
	contentPane.add(Fine);

        BookIdField = new JTextField();
	BookIdField.setForeground(new Color(105, 105, 105));
        //BookIdField.setBackground(Color.red);
	BookIdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	BookIdField.setBounds(100, 52, 70, 20);
	contentPane.add(BookIdField);
	//BookIdField.setColumns(10);

	StudentIdField = new JTextField();
	StudentIdField.setForeground(new Color(105, 105, 105));
        //StudentIdField.setBackground(Color.red);
	StudentIdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	StudentIdField.setBounds(310, 52, 70, 20);
	contentPane.add(StudentIdField);
	//StudentIdField.setColumns(10);

	SearchBtn = new JButton("Search");
        SearchBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
	SearchBtn.addActionListener(this);
	SearchBtn.setBounds(410, 47, 105, 30);
        SearchBtn.setBackground(Color.BLACK);
        SearchBtn.setForeground(Color.WHITE);
	contentPane.add(SearchBtn);

	BookTitleField = new JTextField();
	BookTitleField.setEditable(false);
	BookTitleField.setForeground(new Color(0, 100, 0));
        //BookTitleField.setBackground(Color.red);
	BookTitleField.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
	BookTitleField.setBounds(150, 105, 200, 20);
	contentPane.add(BookTitleField);
	BookTitleField.setColumns(10);

	StudentNameField = new JTextField();
	StudentNameField.setEditable(false);
	StudentNameField.setForeground(new Color(0, 100, 0));
	StudentNameField.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
	StudentNameField.setColumns(10);
	StudentNameField.setBounds(150, 150, 200, 20);
	contentPane.add(StudentNameField);

	IssuedDateField = new JTextField();
	IssuedDateField.setForeground(new Color(0, 100, 0));
	IssuedDateField.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
	IssuedDateField.setEditable(false);
	IssuedDateField.setColumns(10);
	IssuedDateField.setBounds(150, 195, 162, 20);
	contentPane.add(IssuedDateField);
        
        ReturnedDateField = new JTextField();
	ReturnedDateField.setForeground(new Color(0, 100, 0));
	ReturnedDateField.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
        ReturnedDateField.setText(CurrentDate());
	ReturnedDateField.setEditable(false);
	ReturnedDateField.setColumns(10);
	ReturnedDateField.setBounds(150, 240, 162, 20);
	contentPane.add(ReturnedDateField);

//	dateChooser = new JDateChooser();
//	dateChooser.setBorder(new LineBorder(new Color(0, 0, 0), 0, true));
//	dateChooser.setBounds(150, 240, 162, 20);
//	contentPane.add(dateChooser);
        
        FineField = new JTextField();
        FineField.setForeground(new Color(0, 100, 0));
	FineField.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
        FineField.setText("0");
	FineField.setColumns(10);
	FineField.setBounds(150, 285, 162, 20);
	contentPane.add(FineField);
        
        ReturnBtn = new JButton("Return");
        ReturnBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
	ReturnBtn.addActionListener(this);
	ReturnBtn.setBounds(410, 185, 105, 30);
        ReturnBtn.setBackground(Color.BLACK);
        ReturnBtn.setForeground(Color.WHITE);
	contentPane.add(ReturnBtn);

        BackBtn = new JButton("Back");
        BackBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
	BackBtn.addActionListener(this);
	BackBtn.setBounds(410, 237, 105, 30);
        BackBtn.setBackground(Color.BLACK);
        BackBtn.setForeground(Color.WHITE);
	contentPane.add(BackBtn);

        
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
    }
    
    public void actionPerformed(ActionEvent ae){
        try{
            conn con = new conn();
            if(ae.getSource() == SearchBtn){
                //int fine = Integer.parseInt(FineField.getText());
                
                
                String sql = "SELECT * FROM issued_books WHERE Student_Id=? AND Book_Id=?";
		PreparedStatement st = con.c.prepareStatement(sql);
		st.setString(1, StudentIdField.getText());
		st.setString(2, BookIdField.getText());
		ResultSet rs = st.executeQuery();
                int i =0;
                while (rs.next()) {
                    i=1;
                    IssuedDateField.setText(rs.getString("Issue_Date"));
                    String sql2 = "SELECT Name FROM user WHERE Id=?";
                    PreparedStatement st2 = con.c.prepareStatement(sql2);
                    st2.setString(1, StudentIdField.getText());
                    ResultSet rs2 = st2.executeQuery();
                    while(rs2.next()) {
                        i = 1;
                        StudentNameField.setText(rs2.getString("Name"));
                    }
                    String sql3 = "SELECT Book_Title FROM books WHERE Book_Id=?";
                    PreparedStatement st3 = con.c.prepareStatement(sql3);
                    st3.setString(1, BookIdField.getText());
                    ResultSet rs3 = st3.executeQuery();
                    
                    while(rs3.next()) {
                        BookTitleField.setText(rs3.getString("Book_Title"));
                    }
                    
                    
		}
		st.close();
		rs.close();
		if(i==0) {
                     JOptionPane.showMessageDialog(null, "No Issued Record");
                }
            }
            if(ae.getSource() == ReturnBtn){
                if(!(StudentIdField.getText()).isEmpty() && !(BookIdField.getText()).isEmpty() && !(IssuedDateField.getText()).isEmpty())
                {
                    int x=AddFine(StudentIdField.getText());
                    delete();
                    String sql = "INSERT INTO return_books(Student_Id, Book_Id, Issue_Date, Return_Date) values(?, ?, ?, ?)";
                    PreparedStatement st = con.c.prepareStatement(sql);
                    st.setString(1, StudentIdField.getText());
                    st.setString(2, BookIdField.getText());
                    st.setString(3, IssuedDateField.getText());
                    st.setString(4, CurrentDate());
                    int i = st.executeUpdate();
                    if (i > 0) {
                    
                    
                    } else
                        JOptionPane.showMessageDialog(null, "error");
                    }
                     else {
                       JOptionPane.showMessageDialog(null, "Please Fill Required Boxes");
                    }
            }
            if(ae.getSource() == BackBtn){
                this.setVisible(false);
			
            }
        }catch(Exception e){
            
        }
    }
}
