package com.project;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import java.text.SimpleDateFormat;
import java.util.Calendar;



public class AdminHome extends JFrame implements ActionListener{
        final JButton AddBook, Statistics, AddStudent, IssueBook, ReturnBook;
        ImageIcon img = new ImageIcon("bg.png");
        ImageIcon AddBookImg = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/AddBooks.png"));
        ImageIcon StatisticsImg = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/Statistics.png"));
        ImageIcon AddStudentImg= new ImageIcon(ClassLoader.getSystemResource("com/project/icons/AddStudent.png"));
        ImageIcon IssueBookImg = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/IssueBook.png"));
        ImageIcon ReturnBookImg = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/ReturnBook.png"));
        ImageIcon AboutUsImg = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/AboutUs.png"));
 
        public void Reminder() {
            DateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy");
            Date date = new Date();
            String currentDate = dateformat.format(date);
            System.out.println("Current Date: " + currentDate);
            Calendar cal = Calendar.getInstance();                  
            cal.add(Calendar.DAY_OF_MONTH, 1);
            Date futureDate = cal.getTime();
            String tommDate=dateformat.format(futureDate);
            System.out.println("Future Date: " + tommDate);
            try {
                System.out.println("before conn");
                conn con = new conn();
                String query = "SELECT * FROM issued_books WHERE Return_Date=?";
                PreparedStatement st = con.c.prepareStatement(query);
                st.setString(1, tommDate);
                ResultSet rs = st.executeQuery();
                System.out.println("after query");
                if(rs.next()) {
                    System.out.println("inside first if");
                    String emailQuery = "SELECT * FROM user WHERE Id=?";
                    PreparedStatement st2 = con.c.prepareStatement(emailQuery);
                    st2.setString(1, rs.getString("Student_Id"));
                    ResultSet rs2 = st2.executeQuery();
                    if(rs2.next()) {
                        System.out.println("inside second if");
                        String titleQuery = "SELECT * FROM books WHERE Book_Id=?";
                        PreparedStatement st3 = con.c.prepareStatement(titleQuery);
                        st3.setString(1, rs.getString("Book_Id"));
                        ResultSet rs3 = st3.executeQuery();
                        if(rs3.next()) {
                            System.out.println("inside third if");
                            System.out.println("Email" + rs2.getString("Email"));
                            System.out.println("Book Title: " + rs3.getString("Book_Title"));
                            ReminderMail.send(rs2.getString("Email"), rs3.getString("Book_Title"));
                        }
                    }
                }
            } catch(Exception e) {
                
            }
        }
        
	public static void main(String[] args) {
            new AdminHome().setVisible(true);
	}
        
        public AdminHome() {
            setExtendedState(JFrame.MAXIMIZED_BOTH);
            setUndecorated(true);
            setResizable(false);
            Reminder();
            
            JMenuBar menuBar = new JMenuBar();
            menuBar.setBorder(new LineBorder(new Color(176, 176, 176), 2));
            menuBar.setBounds(0, 1, 1600, 35);
            add(menuBar);

            JMenu mnExit = new JMenu("Exit");
            mnExit.setFont(new Font("Trebuchet MS", Font.BOLD, 17));
            
            
            JMenuItem mntmLogout = new JMenuItem("Logout");
            mntmLogout.setBackground(new Color(211, 211, 211));
            mntmLogout.setForeground(new Color(105, 105, 105));
            mntmLogout.addActionListener(this);
            mnExit.add(mntmLogout);
            
            JMenuItem mntmExit = new JMenuItem("Exit");
            mntmExit.setForeground(new Color(105, 105, 105));
            mntmExit.setBackground(new Color(211, 211, 211));
            mntmExit.addActionListener(this);
            mnExit.add(mntmExit);
                
            

            JMenu mnHelp = new JMenu("Help");
            mnHelp.setFont(new Font("Trebuchet MS", Font.BOLD, 17));
            

            JMenuItem mntmReadme = new JMenuItem("Read Me");
            mntmReadme.setBackground(new Color(211, 211, 211));
            mntmReadme.setForeground(new Color(105, 105, 105));
            mnHelp.add(mntmReadme);

            JMenuItem mntmAboutUs = new JMenuItem("About Us");
            mntmAboutUs.setForeground(new Color(105, 105, 105));
            mntmAboutUs.setBackground(new Color(211, 211, 211));
            mntmAboutUs.addActionListener(this);
            mnHelp.add(mntmAboutUs);

            JMenu mnRecord = new JMenu("Record");
            mnRecord.setFont(new Font("Trebuchet MS", Font.BOLD, 17));
            

            JMenuItem bookdetails = new JMenuItem("Book Details");
            bookdetails.addActionListener(this);
            bookdetails.setBackground(new Color(211, 211, 211));
            bookdetails.setForeground(Color.DARK_GRAY);
            mnRecord.add(bookdetails);

            JMenuItem studentdetails = new JMenuItem("Student Details");
            studentdetails.setBackground(new Color(211, 211, 211));
            studentdetails.setForeground(Color.DARK_GRAY);
            studentdetails.addActionListener(this);
            mnRecord.add(studentdetails);
            
            menuBar.add(mnRecord);
            menuBar.add(mnHelp);
            menuBar.add(mnExit);

            
            JLabel l1 = new JLabel("Library Management System");
            l1.setForeground(new Color(255, 10, 0));
            l1.setFont(new Font("Aril", Font.BOLD, 50));
            //l1.setFont(new Font("Aril", Font.BOLD, 50));
            l1.setBounds(450, 60, 750, 80);
            add(l1);
            
            

            JLabel addBookLabel = new JLabel("");
            Image bookImg = AddBookImg.getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT);
            ImageIcon resizeImage = new ImageIcon(bookImg);
            addBookLabel.setIcon(resizeImage);
            addBookLabel.setBounds(440, 220, 159, 152);
            add(addBookLabel);
            
            AddBook = new JButton("Add Books");
            AddBook.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
            AddBook.addActionListener(this);
            AddBook.setBackground(Color.BLACK);
            AddBook.setForeground(Color.WHITE);
            AddBook.setBounds(430, 380, 159, 44);
            add(AddBook);
            
            

            JLabel statisticsLabel = new JLabel("");
            Image statisticsImg = StatisticsImg.getImage().getScaledInstance(140, 140,Image.SCALE_DEFAULT);
            resizeImage = new ImageIcon(statisticsImg);
            statisticsLabel.setIcon(resizeImage);
            statisticsLabel.setBounds(695, 220, 159, 152);
            add(statisticsLabel);
            
            Statistics = new JButton("Statistics");
            Statistics.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
            Statistics.addActionListener(this);
            Statistics.setBackground(Color.BLACK);
            Statistics.setForeground(Color.WHITE);
            Statistics.setBounds(700, 380, 139, 44);
            add(Statistics);
            
            
            
            JLabel addStudentLabel = new JLabel("");
            Image addStudentImg  = AddStudentImg.getImage().getScaledInstance(150, 150,Image.SCALE_DEFAULT);
            resizeImage = new ImageIcon(addStudentImg);
            addStudentLabel.setIcon(resizeImage);
            addStudentLabel .setBounds(955, 220, 225, 152);
            add(addStudentLabel);
            
            AddStudent = new JButton("Add Student");
            AddStudent.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
            AddStudent.addActionListener(this);
            AddStudent.setBackground(Color.BLACK);
            AddStudent.setForeground(Color.WHITE);
            AddStudent.setBounds(945, 380, 167, 44);
            add(AddStudent);

            

//            JPanel panel = new JPanel();
//            panel.setBorder(new TitledBorder(new LineBorder(new Color(255, 50, 0), 4), "Operation", TitledBorder.LEADING,
//				TitledBorder.TOP, new Font("Aril", Font.BOLD, 18), new Color(255, 100, 100)));
//            panel.setBounds(400, 180, 750, 260);
//            panel.setBackground(Color.WHITE);
//            add(panel);
            
            
            
            JLabel issueBookLabel = new JLabel("");
            //ImageIcon i10  = new ImageIcon(ClassLoader.getSystemResource("library/management/system/icons/fifth.png"));
            Image issueBookImg  = IssueBookImg.getImage().getScaledInstance(170, 150,Image.SCALE_DEFAULT);
            resizeImage = new ImageIcon(issueBookImg);
            issueBookLabel.setIcon(resizeImage);
            issueBookLabel.setBounds(590, 510, 180, 163);
            add(issueBookLabel);
            
            IssueBook = new JButton("Issue Book");
            IssueBook.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
            IssueBook.addActionListener(this);
            IssueBook.setBackground(Color.BLACK);
            IssueBook.setForeground(Color.WHITE);
            IssueBook.setBounds(590, 680, 143, 41);
            add(IssueBook);
            
            
            
            JLabel returnBookLabel = new JLabel("");
            Image returnBookImg  = ReturnBookImg.getImage().getScaledInstance(150, 150,Image.SCALE_DEFAULT);
            resizeImage = new ImageIcon(returnBookImg);
            returnBookLabel.setIcon(resizeImage);
            returnBookLabel.setBounds(850, 510, 139, 152);
            add(returnBookLabel);
            
            ReturnBook = new JButton("Return Book");
            ReturnBook.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
            ReturnBook.addActionListener(this);
            ReturnBook.setBackground(Color.BLACK);
            ReturnBook.setForeground(Color.WHITE);
            ReturnBook.setBounds(840, 680, 159, 41);
            add(ReturnBook);
            
            
            
            
//             JLabel aboutLabel = new JLabel("");
//            //ImageIcon i16  = new ImageIcon(ClassLoader.getSystemResource("library/management/system/icons/seventh.png"));
//            Image aboutImg  = AboutUsImg.getImage().getScaledInstance(170, 170,Image.SCALE_DEFAULT);
//            resizeImage = new ImageIcon(aboutImg);
//            aboutLabel.setIcon(resizeImage);
//            aboutLabel.setBounds(950, 510, 157, 152);
//            add(aboutLabel);
//            
//            AboutUs = new JButton("About Us");
//            AboutUs.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
//            AboutUs.addActionListener(this);
//            AboutUs.setBackground(Color.BLACK);
//            AboutUs.setForeground(Color.WHITE);
//            AboutUs.setBounds(960, 680, 159, 41);
//            add(AboutUs);

            

            

            
//            JPanel panel2 = new JPanel();
//            panel2.setBorder(new TitledBorder(new LineBorder(new Color(255, 50, 0), 4), "Action", TitledBorder.LEADING,
//				TitledBorder.TOP, new Font("Aril", Font.BOLD, 18), new Color(255, 100, 100)));
//            panel2.setBounds(400, 480, 750, 270);
//            add(panel2);
            
            
           
            JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
            bgLabel.setBounds(0, 0, 1700, 1000);
            add(bgLabel);
            
	}
        
        
        @Override
        public void actionPerformed(ActionEvent ae){
            String msg = ae.getActionCommand();
            if(msg.equals("Logout")){
                setVisible(false);
		new Login_user().setVisible(true);
            }else if(msg.equals("Exit")){
                System.exit(ABORT);
            
            }else if(msg.equals("Read Me")){
            
            }else if(msg.equals("About Us")){
                setVisible(false);
		//new aboutUs().setVisible(true);
            
            }else if(msg.equals("Book Details")){
                setVisible(false);
		//new BookDetails().setVisible(true);
            }else if(msg.equals("Student Details")){
                setVisible(false);
               // new StudentDetails().setVisible(true);
			
            }
            
            if(ae.getSource() == AddBook){
                new AddBook().setVisible(true);
            }
            if(ae.getSource() == Statistics){
                new Statistics().setVisible(true);
            }
            if(ae.getSource() == AddStudent){
                new AddStudent().setVisible(true);
            }
            if(ae.getSource() == IssueBook){
                new IssueBook().setVisible(true);
            }
            if(ae.getSource() == ReturnBook){
                new ReturnBook().setVisible(true);
            
            }
        }
}
