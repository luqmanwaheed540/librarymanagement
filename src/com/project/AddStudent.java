package com.project;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

public class AddStudent extends JFrame implements ActionListener{

    private JPanel contentPane;
    private JTextField IdField, NameField, FatherNameField, EmailField;
    private JPasswordField PasswordField;
    JButton b1,b2;

    public static void main(String[] args) {
        new AddStudent().setVisible(true);
    }


    public AddStudent() {
        //super("Add Student");
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
        
	contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setLayout(null);
	contentPane.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 5, true), "Add Student",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
	//setContentPane(contentPane);
        contentPane.setBounds(100,50,450,380);
        add(contentPane);

	JLabel l1 = new JLabel("Sap Id");
	l1.setForeground(Color.WHITE);
	l1.setFont(new Font("Tahoma", Font.BOLD, 14));
	l1.setBounds(40, 50, 102, 22);
	contentPane.add(l1);

	JLabel l2 = new JLabel("Full Name");
	l2.setForeground(Color.WHITE);
	l2.setFont(new Font("Tahoma", Font.BOLD, 14));
	l2.setBounds(40, 100, 102, 22);
	contentPane.add(l2);

	JLabel l3 = new JLabel("Father's Name");
	l3.setForeground(Color.WHITE);
	l3.setFont(new Font("Tahoma", Font.BOLD, 14));
	l3.setBounds(40, 150, 102, 22);
	contentPane.add(l3);
        
        JLabel l7 = new JLabel("Email");
	l7.setForeground(Color.WHITE);
	l7.setFont(new Font("Tahoma", Font.BOLD, 14));
	l7.setBounds(40, 200, 102, 22);
	contentPane.add(l7);

	JLabel l4 = new JLabel("Password");
	l4.setForeground(Color.WHITE);
	l4.setFont(new Font("Tahoma", Font.BOLD, 14));
	l4.setBounds(40, 250, 102, 22);
	contentPane.add(l4);


	IdField = new JTextField();
	IdField.setForeground(new Color(47, 79, 79));
	IdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	IdField.setBounds(180, 50, 210, 20);
	contentPane.add(IdField);
	IdField.setColumns(10);

	NameField = new JTextField();
	NameField.setForeground(new Color(47, 79, 79));
	NameField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	NameField.setColumns(10);
	NameField.setBounds(180, 100, 210, 20);
	contentPane.add(NameField);

	FatherNameField = new JTextField();
	FatherNameField.setForeground(new Color(47, 79, 79));
	FatherNameField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	FatherNameField.setColumns(10);
	FatherNameField.setBounds(180, 150, 210, 20);
	contentPane.add(FatherNameField);
        
        EmailField = new JTextField();
	EmailField.setForeground(new Color(47, 79, 79));
	EmailField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	EmailField.setColumns(10);
	EmailField.setBounds(180, 200, 210, 20);
	contentPane.add(EmailField);
        
        PasswordField = new JPasswordField();
	PasswordField.setForeground(new Color(47, 79, 79));
	PasswordField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	PasswordField.setColumns(10);
	PasswordField.setBounds(180, 250, 210, 20);
	contentPane.add(PasswordField);

	b1 = new JButton("ADD");
	b1.addActionListener(this);
	b1.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	b1.setBounds(120, 315, 111, 33);
        b1.setBackground(Color.BLACK);
        b1.setForeground(Color.WHITE);
	contentPane.add(b1);

	b2 = new JButton("Back");
	b2.addActionListener(this);
	b2.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	b2.setBounds(250, 315, 111, 33);
        b2.setBackground(Color.BLACK);
        b2.setForeground(Color.WHITE);
	contentPane.add(b2);


	JPanel panel = new JPanel();
	panel.setBorder(new TitledBorder(new LineBorder(new Color(102, 205, 170), 2, true), "Add-Student",
			TitledBorder.LEADING, TitledBorder.TOP, null, new Color(30, 144, 255)));
	panel.setBackground(new Color(211, 211, 211));
        
        contentPane.setBackground(Color.WHITE);
        panel.setBackground(Color.WHITE);
        
	contentPane.add(panel);
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        try{
            
            if(ae.getSource() == b1){
                try{
                conn con = new conn();
                String sql = "INSERT into user(Id, Name, Father_Name, Email, Password, Role, Fine) values(?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement st = con.c.prepareStatement(sql);
		st.setString(1, IdField.getText());
		st.setString(2, NameField.getText());
		st.setString(3, FatherNameField.getText());
                st.setString(4, EmailField.getText());
                st.setString(5, PasswordField.getText());
                st.setString(6, "1");
                st.setString(7, "0");

		int i = st.executeUpdate();
		if (i > 0){
                    JOptionPane.showMessageDialog(null, "Student Added Successfully");
                    this.setVisible(false);
                    new AdminHome().setVisible(true);
                }
		else
                    JOptionPane.showMessageDialog(null, "error");
                }catch(HeadlessException | SQLException e){
                }
            }
            
            if(ae.getSource() == b2){
                this.setVisible(false);		
            }
        }catch(Exception e){
            
        }
    }
}

