/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

/**
 *
 * @author Luqman Waheed
 */
public class OTP extends JFrame implements ActionListener {
    private String otp;
    private String OTPSend;
    //private JPanel codePanel;
    private JLabel codeStatement;
    private JTextField code1Field, code2Field, code3Field, code4Field, OTPField;
    private JButton SubmitBtn, btn;
    
    public OTP(String otp) {
        JOptionPane.showMessageDialog(null, "Code Send To Your Email.....");
        OTPSend = otp;
        setBounds(500, 200, 518, 442);
        setResizable(false);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
//        codePanel = new JPanel();
//        codePanel.setLayout(null);
        
           codeStatement = new JLabel("Enter Code");
           codeStatement.setFont(new Font("Arial", Font.BOLD, 24));
           codeStatement.setOpaque(true);
           codeStatement.setBackground(new Color(176, 176, 176));
           codeStatement.setBounds(170,50,130,40);
           add(codeStatement);
           
           code1Field = new JTextField();
           code1Field.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           //code1Field.setDocument(new JTextFieldLimit(1));
           code1Field.setBounds(110, 120, 40, 40);
           add(code1Field);
           
           JLabel sign1 = new JLabel("_");
           sign1.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           sign1.setBounds(155, 110, 40, 40);
           add(sign1);
           
           code2Field = new JTextField();
           code2Field.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           code2Field.setDocument(new JTextFieldLimit(1));
           code2Field.setBounds(175, 120, 40, 40);
           add(code2Field);
           
           JLabel sign2 = new JLabel("_");
           sign2.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           sign2.setBounds(220, 110, 40, 40);
           add(sign2);
           
           code3Field = new JTextField();
           code3Field.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           code3Field.setDocument(new JTextFieldLimit(1));
           code3Field.setBounds(240, 120, 40, 40);
           add(code3Field);
           
           JLabel sign3 = new JLabel("_");
           sign3.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           sign3.setBounds(285, 110, 40, 40);
           add(sign3);
           
           code4Field = new JTextField();
           code4Field.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           code4Field.setDocument(new JTextFieldLimit(1));
           code4Field.setBounds(305, 120, 40, 40);
           add(code4Field);
           
           
           OTPField = new JTextField();
           OTPField.setFont(new Font("Arial", Font.CENTER_BASELINE, 20));
           OTPField.setDocument(new JTextFieldLimit(1));
           OTPField.setText(otp);
           OTPField.setBounds(305, 120, 40, 40);
           add(OTPField);
           
           
           
           
           SubmitBtn = new JButton("Submit");
           SubmitBtn.addActionListener(this);
           SubmitBtn.setBackground(new Color(211, 211, 211));
           SubmitBtn.setForeground(Color.BLACK);
           SubmitBtn.setBounds(180, 180, 100, 30);
           add(SubmitBtn);
}

    @Override
    public void actionPerformed(ActionEvent e) {
        otp = code1Field.getText() + code2Field.getText() + code3Field.getText() + code4Field.getText();
        try {
            if(otp.equals(OTPSend)) {
                setVisible(false);
                new NewPassword().setVisible(true);
            }
            else {
                JOptionPane.showMessageDialog(null, "Invalid Code...!.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e2) {
                    e2.printStackTrace();
		}
    }
    
    public static void main(String[] args) {
        
    }
    
}
