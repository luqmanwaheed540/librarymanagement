/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Luqman Waheed
 */

class JTextFieldLimit extends PlainDocument {
   private int limit;
   JTextFieldLimit(int limit) {
      super();
      this.limit = limit;
   }
   JTextFieldLimit(int limit, boolean upper) {
      super();
      this.limit = limit;
   }
   public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
      if (str == null)
         return;
      if ((getLength() + str.length()) <= limit) {
         super.insertString(offset, str, attr);
      }
   }
}

public class ForgetPassword extends JFrame implements ActionListener{
    private String OTP = "";
    private JPanel verifyEmailPanel, newPassword; // codePanel,
    private JTextField emailField; //code1Field, code2Field, code3Field, code4Field;
    private JButton VerifyBtn, BacBtn;
    public ForgetPassword() {
           setBounds(500, 200, 518, 442);
           setResizable(false);
           //setUndecorated(true);
           setLayout(null);
           setDefaultCloseOperation(EXIT_ON_CLOSE);
           
           verifyEmailPanel = new JPanel();
           setContentPane(verifyEmailPanel);
           verifyEmailPanel.setLayout(null);
           
           
           
           //--------- Verify Email Panel Data ---------- 
           JLabel emailStatement = new JLabel("Enter Email You Have Provided");
           emailStatement.setFont(new Font("Arial", Font.BOLD, 24));
           emailStatement.setOpaque(true);
           emailStatement.setBackground(new Color(176, 176, 176));
           emailStatement.setBounds(60,50,360,40);
           verifyEmailPanel.add(emailStatement);
           
           JLabel email = new JLabel("Email: ");
           email.setFont(new Font("Arial", Font.PLAIN, 14));
           email.setBounds(90,140,60,10);
           verifyEmailPanel.add(email);
           //add(email);
           
           emailField = new JTextField();
           emailField.setFont(new Font("Arial", Font.PLAIN, 15));
           emailField.setBounds(140, 130, 200, 30);
           verifyEmailPanel.add(emailField);
           //add(emailField);
           
           VerifyBtn = new JButton("Verify");
           VerifyBtn.addActionListener(this);
           VerifyBtn.setBackground(new Color(211, 211, 211));
           VerifyBtn.setForeground(Color.BLACK);
           VerifyBtn.setBounds(240, 180, 100, 30);
           verifyEmailPanel.add(VerifyBtn);
           
          
           BacBtn = new JButton("Back");
           BacBtn.addActionListener(this);
           BacBtn.setBackground(new Color(211, 211, 211));
           BacBtn.setForeground(Color.BLACK);
           BacBtn.setBounds(120, 180, 100, 30);
           verifyEmailPanel.add(BacBtn);
           
           
           
           
           //getContentPane().setBackground(Color.BLACK);
           //verifyEmailPanel.setBackground(Color.BLACK);
           
           //getContentPane().setBackground(Color.WHITE);
           //codePanel.setBackground(Color.WHITE);
           
         
    }
    public static void main(String[] args) {
        new ForgetPassword().setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == VerifyBtn) {
            Boolean status = false;
		try {
                    conn con = new conn();
                    String sql = "SELECT * FROM user WHERE Email=?";
                    PreparedStatement st = con.c.prepareStatement(sql);
                    
                    st.setString(1, emailField.getText());

                    ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        setVisible(false);
                        SendMail.send(emailField.getText());
                    }
            else {
               JOptionPane.showMessageDialog(null, "Invalid Email...!.", "Error", JOptionPane.ERROR_MESSAGE);
                verifyEmailPanel.setVisible(true);
            }       
	} catch (Exception e2) {
                    e2.printStackTrace();
		}
        }
        if(e.getSource() == BacBtn) {
            setVisible(false);
            new Login_user().setVisible(true);
        }
    }
}

