/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import javax.mail.*;
import java.util.Properties;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import java.util.Random;

/**
 *
 * @author Luqman Waheed
 */
public class SendMail {
    private static String OTP = "";
    public static void send(String recepient) throws MessagingException {
        Properties properties = new Properties();
        
        properties.put("mail.smtp.auth","true");
        properties.put("mail.smtp.starttls.enable","true");
        properties.put("mail.smtp.host","smtp.gmail.com");
        properties.put("mail.smtp.port","587");
        
        String email = "luqmanwaheed540@gmail.com";
        String password = "R@walpindi1";
        
        Session session = Session.getInstance(properties, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });
        Message message = prepareMessage(session, email, recepient);
        Transport.send(message);
    }
    private static Message prepareMessage(Session session, String email, String recepient) {
            try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject("Mail From Library Management System");
            Random rand = new Random();
            int n = rand.nextInt(1000,9999);
            OTP = Integer.toString(n);
            message.setText("Your 4-digit code is " + OTP);
            //JOptionPane.showMessageDialog(null, "Code Send To Your Email.....");
            new OTP(OTP).setVisible(true);
            return message;
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Error Occured, Try Again!...", "Error", JOptionPane.ERROR_MESSAGE);
            
        }
            return null;
    }
    public String getOTP()
    {
        return OTP;
    }
    
}
