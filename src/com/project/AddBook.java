package com.project;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

public class AddBook extends JFrame implements ActionListener{

    final JPanel contentPane;
    final JTextField IdField, TitleField, AuthurField, PublisherField, PriceField, PagesField, stockField;
    final JButton AddBtn, BackBtn;
    JComboBox EditionBox;
        
    public static void main(String[] args) {
	new AddBook().setVisible(true);
    }

    public AddBook() {
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
	contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setLayout(null);
	contentPane.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 5, true), "Add Book",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        contentPane.setBounds(50,50,600,380);
        add(contentPane);
        
        
        JLabel l6 = new JLabel("Id");
	l6.setForeground(new Color(47, 79, 79));
	l6.setFont(new Font("Tahoma", Font.BOLD, 14));
        l6.setForeground(Color.WHITE);
	l6.setBounds(30, 80, 90, 22);
	contentPane.add(l6);
        
        IdField = new JTextField();
	IdField.setForeground(new Color(47, 79, 79));
	IdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	IdField.setColumns(10);
	IdField.setBounds(110, 80, 170, 20);
	contentPane.add(IdField);

	JLabel l1 = new JLabel("Title");
	l1.setForeground(new Color(47, 79, 79));
	l1.setFont(new Font("Tahoma", Font.BOLD, 14));
        l1.setForeground(Color.WHITE);
	l1.setBounds(30, 130, 90, 22);
	contentPane.add(l1);
        
        TitleField = new JTextField();
	TitleField.setForeground(new Color(47, 79, 79));
	TitleField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	TitleField.setColumns(10);
	TitleField.setBounds(110, 130, 170, 20);
	contentPane.add(TitleField);
        
        
	JLabel l2 = new JLabel("Auther");
	l2.setForeground(new Color(47, 79, 79));
	l2.setFont(new Font("Tahoma", Font.BOLD, 14));
        l2.setForeground(Color.WHITE);
	l2.setBounds(30, 180, 90, 22);
	contentPane.add(l2);
      
        AuthurField = new JTextField();
	AuthurField.setForeground(new Color(47, 79, 79));
	AuthurField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	AuthurField.setColumns(10);
	AuthurField.setBounds(110, 180, 170, 20);
	contentPane.add(AuthurField);

	JLabel l3 = new JLabel("Publisher");
	l3.setForeground(new Color(47, 79, 79));
	l3.setFont(new Font("Tahoma", Font.BOLD, 14));
        l3.setForeground(Color.WHITE);
	l3.setBounds(30, 230, 90, 22);
	contentPane.add(l3);
        
        PublisherField = new JTextField();
	PublisherField.setForeground(new Color(47, 79, 79));
	PublisherField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	PublisherField.setColumns(10);
	PublisherField.setBounds(110, 230, 170, 20);
	contentPane.add(PublisherField);
        
        JLabel l7 = new JLabel("Edition");
	l7.setForeground(new Color(47, 79, 79));
	l7.setFont(new Font("Tahoma", Font.BOLD, 14));
        l7.setForeground(Color.WHITE);
	l7.setBounds(370, 80, 90, 22);
	contentPane.add(l7);
        
        EditionBox = new JComboBox();
	EditionBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
	EditionBox.setBounds(450, 80, 120, 20);
	contentPane.add(EditionBox);

	JLabel l4 = new JLabel("Price");
	l4.setForeground(new Color(47, 79, 79));
	l4.setFont(new Font("Tahoma", Font.BOLD, 14));
        l4.setForeground(Color.WHITE);
	l4.setBounds(370, 130, 90, 22);
	contentPane.add(l4);
        
        PriceField = new JTextField();
	PriceField.setForeground(new Color(47, 79, 79));
	PriceField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	PriceField.setColumns(10);
	PriceField.setBounds(450, 130, 120, 20);
	contentPane.add(PriceField);

	JLabel l5 = new JLabel("Pages");
	l5.setForeground(new Color(47, 79, 79));
	l5.setFont(new Font("Tahoma", Font.BOLD, 14));
        l5.setForeground(Color.WHITE);
	l5.setBounds(370, 180, 90, 22);
	contentPane.add(l5);
        
        PagesField = new JTextField();
	PagesField.setForeground(new Color(47, 79, 79));
	PagesField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	PagesField.setColumns(10);
	PagesField.setBounds(450, 180, 120, 20);
	contentPane.add(PagesField);
        
        
        JLabel l8 = new JLabel("Stock");
	l8.setForeground(new Color(47, 79, 79));
	l8.setFont(new Font("Tahoma", Font.BOLD, 14));
        l8.setForeground(Color.WHITE);
	l8.setBounds(370, 230, 90, 22);
	contentPane.add(l8);
        
        stockField = new JTextField();
	stockField.setForeground(new Color(47, 79, 79));
	stockField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	stockField.setColumns(10);
	stockField.setBounds(450, 230, 120, 20);
	contentPane.add(stockField);
        
        AddBtn = new JButton("Add");
        AddBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        AddBtn.addActionListener(this);
        AddBtn.setBackground(Color.BLACK);
        AddBtn.setForeground(Color.WHITE);
        AddBtn.setBounds(180, 300, 120, 35);
        contentPane.add(AddBtn);

        
        BackBtn = new JButton("Back");
        BackBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        BackBtn.addActionListener(this);
        BackBtn.setBackground(Color.BLACK);
        BackBtn.setForeground(Color.WHITE);
        BackBtn.setBounds(320, 300, 120, 35);
        contentPane.add(BackBtn);
	

        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 800, 800);
        add(bgLabel);
    }
    
    public void actionPerformed(ActionEvent ae){
        try{
            conn con = new conn();
            if(ae.getSource() == AddBtn){
                String queryId = "SELECT * FROM books WHERE Book_Id=?";
                PreparedStatement st = con.c.prepareStatement(queryId);
                st.setString(1, IdField.getText());
                 ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        JOptionPane.showMessageDialog(null, "Book Id Exist...!.", "Error", JOptionPane.ERROR_MESSAGE);
                        this.setVisible(true);
                    } else {
                        if(!IdField.getText().isEmpty() && !TitleField.getText().isEmpty() && !AuthurField.getText().isEmpty() && !PublisherField.getText().isEmpty()&& !PriceField.getText().isEmpty() && !PagesField.getText().isEmpty() && !stockField.getText().isEmpty()) 
                        {
                            try {
                            String AddBook = "INSERT into books(Book_Id, Book_Title, Book_Author, Publisher, Edition, Price, Total_Pages, Total_Stock) values(?, ?, ?, ?, ?, ?, ?, ?)";
                            st = con.c.prepareStatement(AddBook);
                            st.setString(1, IdField.getText());
                            st.setString(2, TitleField.getText());
                            st.setString(3, AuthurField.getText());
                            st.setString(4, PublisherField.getText());
                            st.setString(5, (String) EditionBox.getSelectedItem());
                            st.setString(6, PriceField.getText());
                            st.setString(7, PagesField.getText());
                            st.setString(7, stockField.getText());
                            int result = st.executeUpdate();
                            if (result > 0)
                            {
                                JOptionPane.showMessageDialog(null, "Successfully Added");
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(null, "Error 404...!.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                             } catch(HeadlessException | SQLException e) {
                                 JOptionPane.showMessageDialog(null, "Error 404...!.", "Error", JOptionPane.ERROR_MESSAGE);
                                     }
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "Fields Cannot Be Empty...!.", "Error", JOptionPane.ERROR_MESSAGE);
                            
                        }
                        st.close();
                    }
                
                
                
                //String sql2 = "insert into books(Book_Id, Book_Title, Book_Author, Publisher, Edition, Price, Total_Pages) values(?, ?, ?, ?, ?, ?, ?)";
		//PreparedStatement st = con.c.prepareStatement(sql2);
                // st.setInt(1, Integer.parseInt(textField.getText()));
		
            }
            if(ae.getSource() == BackBtn){
                this.setVisible(false);
		//new Home().setVisible(true);
            }
            con.c.close();
        }catch(HeadlessException | SQLException e){
        }
    }
}
