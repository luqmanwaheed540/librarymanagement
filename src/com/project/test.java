/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.Color;
import java.awt.Font;
import javax.swing.*;

/**
 *
 * @author Luqman Waheed
 */
public class test extends JFrame{
    private JPanel panel;
	private JTextField sapID;
	private JPasswordField password;
        private JButton login, forget;
        private ImageIcon resizeImage;
    private JLabel bgLabel;
    public test() {
    setExtendedState(JFrame.MAXIMIZED_BOTH);
    setResizable(false);
    //setSize(1000,1000);
    setLayout(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    
    
    
    
    
    panel = new JPanel();
	//panel.setBackground(new Color(176, 224, 230));
	setContentPane(panel);
	panel.setLayout(null);
        
        
        JLabel heading = new JLabel(" Login Form");
        heading.setFont(new Font("Arial", Font.BOLD, 50));
        heading.setOpaque(true);
        heading.setForeground(Color.WHITE);
        heading.setBackground(new Color(176, 176, 176));
	heading.setBounds(650, 100, 300, 80);
	panel.add(heading);
        
	JLabel l1 = new JLabel("Sap ID : ");
        l1.setFont(new Font("Arial", Font.PLAIN, 20));
        l1.setForeground(Color.WHITE);
	l1.setBounds(590, 300, 100, 24);
	panel.add(l1);

	JLabel l2 = new JLabel("Password : ");
        l2.setFont(new Font("Arial", Font.PLAIN, 20));
        l2.setForeground(Color.WHITE);
	l2.setBounds(565, 350, 120, 24);
	panel.add(l2);

	sapID = new JTextField();
	sapID.setBounds(680, 295, 250, 30);
        sapID.setFont(new Font("Arial", Font.PLAIN, 15));
	panel.add(sapID);
	
	password= new JPasswordField();
	password.setBounds(680, 345, 250, 30);
        password.setFont(new Font("Arial", Font.PLAIN, 15));
	panel.add(password);
}
    public static void main(String[] args) {
        new test().setVisible(true);
    }
}
