/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.Color;
import java.awt.Font;
import javax.swing.*;
import javax.swing.border.LineBorder;

/**
 *
 * @author Luqman Waheed
 */
public class StudentHome extends JFrame {
    JButton Detail;
    JPanel panel;
    public StudentHome() {
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
        panel = new JPanel();
        panel.setBackground(Color.red);
        panel.setBounds(100, 50, 300,200);
        JLabel label = new JLabel("HELLO STUDENT");
        label.setForeground(Color.WHITE);
        label.setBounds(10,20,100,30);
        panel.add(label);
        
        Detail = new JButton("Back");
	Detail.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
        //Detail.addActionListener(this);
        Detail.setBackground(Color.BLACK);
        Detail.setForeground(Color.WHITE);
	Detail.setBorder(new LineBorder(new Color(192, 192, 192), 1, true));
	Detail.setBounds(200, 300, 120, 50);
        panel.add(Detail);
        add(panel);
        //setVisible(true);
        
        
        
        
        
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
    }
    public static void main(String[] args) {
        new StudentHome().setVisible(true);
    }
}
