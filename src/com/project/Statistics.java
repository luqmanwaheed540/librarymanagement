/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Luqman Waheed
 */
public class Statistics extends JFrame implements ActionListener {
    JPanel IssuedPanel, ReturnPanel;
    JTable IssuedTable, ReturnTable;
    
    
    public void issueBook() {
	try {
            conn con =  new conn();
            String sql = "select * from issued_books ORDER BY Issue_Date ASC";
            PreparedStatement st = con.c.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            IssuedTable.setModel(DbUtils.resultSetToTableModel(rs));

	} catch (Exception e) {
			// TODO: handle exception
	}
    }
    public void returnBook() {
        try {
            conn con = new conn();
            String sql = "SELECT * FROM return_books ORDER BY Return_Date DESC";
            PreparedStatement st = con.c.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            ReturnTable.setModel(DbUtils.resultSetToTableModel(rs));
	} catch (Exception e) {
			// TODO: handle exception
	}
    }
    
    public Statistics() {
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
        
        
        JLabel l1 = new JLabel("Back");
	l1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
		setVisible(false);
            }
	});
	l1.setForeground(Color.WHITE);
	l1.setFont(new Font("Tahoma", Font.BOLD, 18));
	ImageIcon i1 = new ImageIcon(ClassLoader.getSystemResource("com/project/icons/Back.png"));
        Image i2 = i1.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        ImageIcon i3 = new ImageIcon(i2);
        l1.setIcon(i3);
	l1.setBounds(585, 30, 90, 27);
	add(l1);
        
        IssuedPanel = new JPanel();
        IssuedPanel.setOpaque(false);
        IssuedPanel.setLayout(null);
        IssuedPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 4, true), "Issued Book Detail",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        IssuedPanel.setBounds(20, 70, 650, 200);
        add(IssuedPanel);
        
        JScrollPane issuedSPanel = new JScrollPane();
	issuedSPanel.setBounds(20, 30, 610, 150);
        //scrollPane_1.setOpaque(false);
	IssuedPanel.add(issuedSPanel);

	IssuedTable = new JTable();
	IssuedTable.setBackground(new Color(204, 255, 255));
	IssuedTable.setForeground(new Color(153, 51, 0));
        //IssuedTable.setOpaque(false);
	IssuedTable.setFont(new Font("Tahoma", Font.BOLD, 12));
	issuedSPanel.setViewportView(IssuedTable);
        issueBook();
        
        
        
        
        
        
        
        
        ReturnPanel = new JPanel();
        ReturnPanel.setOpaque(false);
        ReturnPanel.setLayout(null);
        ReturnPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 4, true), "Returned Book Detail",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        ReturnPanel.setBounds(20, 280, 650, 200);
        add(ReturnPanel);
        
        JScrollPane returnedSPanel = new JScrollPane();
	returnedSPanel.setBounds(20, 30, 610, 150);
        //scrollPane_1.setOpaque(false);
	ReturnPanel.add(returnedSPanel);

	ReturnTable = new JTable();
	ReturnTable.setBackground(new Color(204, 255, 255));
	ReturnTable.setForeground(new Color(153, 51, 0));
        //IssuedTable.setOpaque(false);
	ReturnTable.setFont(new Font("Tahoma", Font.BOLD, 12));
	returnedSPanel.setViewportView(ReturnTable);
        returnBook();
        
        
        
        
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
}

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static void main(String[] args) {
       new Statistics().setVisible(true);
    }
}
