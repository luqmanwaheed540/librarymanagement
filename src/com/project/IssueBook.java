/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.project;



import com.toedter.calendar.JDateChooser;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.border.LineBorder;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;
import java.util.Date;
/**
 *
 * @author Luqman Waheed
 */
public class IssueBook extends JFrame implements ActionListener{
    JPanel bookPanel, studentPanel;
    JTextField BookIdField, StudentIdField, BookTitleField, BookAuthorField, PublisherField, EditionField, PriceField, PagesField, AvailableField,
            NameField, FatherNameField, EmailField, FineField, IssuedDateField;
    final JButton SearchBookBtn, SearchStudentBtn, BackBtn, IssueBtn;
    //JDateChooser dateChooser;
    
    
    String CurrentDate() {
        DateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy");
            Date date = new Date();
            String currentDate = dateformat.format(date);
            return currentDate;
    }
    
    IssueBook() {
        setBounds(430, 215, 690, 510);
        setUndecorated(true);
        setResizable(false);
        
        
        bookPanel = new JPanel();
        bookPanel.setOpaque(false);
        bookPanel.setLayout(null);
        bookPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 4, true), "Book Detail",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        //bookPanel.setBackground(null);
        bookPanel.setBounds(10, 50, 310, 370);
        add(bookPanel);
        
        
        
        JLabel BookIdLabel = new JLabel("Book Id");
	BookIdLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	BookIdLabel.setForeground(Color.WHITE);
	BookIdLabel.setBounds(18, 40, 100, 23);
	bookPanel.add(BookIdLabel);

	JLabel TitleLabel = new JLabel("Title");
	TitleLabel.setForeground(Color.WHITE);
	TitleLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	TitleLabel.setBounds(18, 80, 100, 23);
	bookPanel.add(TitleLabel);

	JLabel AuthorLabel = new JLabel("Author");
	AuthorLabel.setForeground(Color.WHITE);
	AuthorLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	AuthorLabel.setBounds(18, 120, 100, 23);
	bookPanel.add(AuthorLabel);

	JLabel PublisherLabel = new JLabel("Publisher");
	PublisherLabel.setForeground(Color.WHITE);
	PublisherLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	PublisherLabel.setBounds(18, 160, 100, 23);
	bookPanel.add(PublisherLabel);

	JLabel EditionLabel = new JLabel("Edition");
	EditionLabel.setForeground(Color.WHITE);
	EditionLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	EditionLabel.setBounds(18, 200, 100, 23);
	bookPanel.add(EditionLabel);

	JLabel PriceLabel = new JLabel("Price");
	PriceLabel.setForeground(Color.WHITE);
	PriceLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	PriceLabel.setBounds(18, 240, 100, 23);
	bookPanel.add(PriceLabel);

	JLabel PagesLabel = new JLabel("Total Pages");
	PagesLabel.setForeground(Color.WHITE);
	PagesLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	PagesLabel.setBounds(18, 280, 100, 23);
	bookPanel.add(PagesLabel);
        
        JLabel StockLabel = new JLabel("Remaining Stock");
	StockLabel.setForeground(Color.WHITE);
	StockLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	StockLabel.setBounds(18, 320, 140, 23);
	bookPanel.add(StockLabel);
        
        BookIdField = new JTextField();
	BookIdField.setForeground(new Color(47, 79, 79));
	BookIdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	BookIdField.setBounds(90, 40, 90, 20);
	bookPanel.add(BookIdField);
	
	SearchBookBtn = new JButton("Search");
	SearchBookBtn.addActionListener(this);
	SearchBookBtn.setBorder(new LineBorder(new Color(192, 192, 192), 1, true));
        SearchBookBtn.setBackground(Color.BLACK);
        SearchBookBtn.setForeground(Color.WHITE);
	SearchBookBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	SearchBookBtn.setBounds(200, 35, 90, 30);
        bookPanel.add(SearchBookBtn);
        
        BookTitleField = new JTextField();
	BookTitleField.setEditable(false);
	BookTitleField.setForeground(new Color(47, 79, 79));
        BookTitleField.setBackground(new Color(210, 210, 210));
	BookTitleField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	BookTitleField.setBounds(100, 80, 190, 20);
	bookPanel.add(BookTitleField);
	BookTitleField.setColumns(10);

	BookAuthorField = new JTextField();
	BookAuthorField.setEditable(false);
	BookAuthorField.setForeground(new Color(47, 79, 79));
        BookAuthorField.setBackground(new Color(210, 210, 210));
	BookAuthorField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	BookAuthorField.setColumns(10);
	BookAuthorField.setBounds(100, 120, 190, 20);
	bookPanel.add(BookAuthorField);

	PublisherField = new JTextField();
	PublisherField.setEditable(false);
	PublisherField.setForeground(new Color(47, 79, 79));
        PublisherField.setBackground(new Color(210, 210, 210));
	PublisherField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	PublisherField.setColumns(10);
	PublisherField.setBounds(100, 160, 190, 20);
	bookPanel.add(PublisherField);

	EditionField = new JTextField();
	EditionField.setEditable(false);
	EditionField.setForeground(new Color(47, 79, 79));
        EditionField.setBackground(new Color(210, 210, 210));
	EditionField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	EditionField.setColumns(10);
	EditionField.setBounds(100, 200, 190, 20);
	bookPanel.add(EditionField);

	PriceField = new JTextField();
	PriceField.setEditable(false);
	PriceField.setForeground(new Color(47, 79, 79));
        PriceField.setBackground(new Color(210, 210, 210));
	PriceField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	PriceField.setColumns(10);
	PriceField.setBounds(100, 240, 190, 20);
	bookPanel.add(PriceField);

	PagesField = new JTextField();
	PagesField.setEditable(false);
	PagesField.setForeground(new Color(47, 79, 79));
        PagesField.setBackground(new Color(210, 210, 210));
	PagesField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	PagesField.setColumns(10);
	PagesField.setBounds(120, 280, 170, 20);
	bookPanel.add(PagesField);
        
        AvailableField = new JTextField();
	AvailableField.setEditable(false);
	AvailableField.setForeground(new Color(47, 79, 79));
        AvailableField.setBackground(new Color(210, 210, 210));
	AvailableField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	AvailableField.setColumns(10);
	AvailableField.setBounds(155, 320, 135, 20);
	bookPanel.add(AvailableField);
        
 
        
        studentPanel = new JPanel();
        studentPanel.setOpaque(false);
        studentPanel.setLayout(null);
        studentPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 69, 0), 4, true), "Student Detail",
			TitledBorder.LEADING, TitledBorder.TOP, new Font("Tahoma", Font.BOLD, 16), new Color(255, 69, 0)));
        studentPanel.setBackground(null);
        studentPanel.setBounds(330, 50, 350, 250);
        add(studentPanel);
        
        JLabel StudentIdLabel = new JLabel("Student Id");
	StudentIdLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	StudentIdLabel.setForeground(Color.WHITE);
	StudentIdLabel.setBounds(18, 40, 100, 23);
	studentPanel.add(StudentIdLabel);
        
        JLabel NameLabel = new JLabel("Student Name");
	NameLabel.setForeground(Color.WHITE);
	NameLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	NameLabel.setBounds(18, 80, 100, 23);
	studentPanel.add(NameLabel);
           
        JLabel FNameLabel = new JLabel("Father Name");
	FNameLabel.setForeground(Color.WHITE);
	FNameLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	FNameLabel.setBounds(18, 120, 100, 23);
	studentPanel.add(FNameLabel);

	JLabel EmailLabel = new JLabel("Email");
	EmailLabel.setForeground(Color.WHITE);
	EmailLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	EmailLabel.setBounds(18, 160, 100, 23);
	studentPanel.add(EmailLabel);

	JLabel FineLabel = new JLabel("Fine");
	FineLabel.setForeground(Color.WHITE);
	FineLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	FineLabel.setBounds(18, 200, 100, 23);
	studentPanel.add(FineLabel);
        
        
        StudentIdField = new JTextField();
	StudentIdField.setForeground(new Color(47, 79, 79));
	StudentIdField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	StudentIdField.setBounds(110, 40, 100, 20);
	studentPanel.add(StudentIdField);
	
	SearchStudentBtn = new JButton("Search");
	SearchStudentBtn.addActionListener(this);
	SearchStudentBtn.setBorder(new LineBorder(new Color(192, 192, 192), 1, true));
        SearchStudentBtn.setBackground(Color.BLACK);
        SearchStudentBtn.setForeground(Color.WHITE);
	SearchStudentBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	SearchStudentBtn.setBounds(240, 35, 90, 30);
        studentPanel.add(SearchStudentBtn);
        
        
        NameField = new JTextField();
	NameField.setEditable(false);
	NameField.setForeground(new Color(47, 79, 79));
        NameField.setBackground(new Color(210, 210, 210));
	NameField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	NameField.setBounds(130, 82, 200, 20);
	studentPanel.add(NameField);
	NameField.setColumns(10);

	FatherNameField = new JTextField();
	FatherNameField.setEditable(false);
	FatherNameField.setForeground(new Color(47, 79, 79));
        FatherNameField.setBackground(new Color(210, 210, 210));
	FatherNameField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	FatherNameField.setColumns(10);
	FatherNameField.setBounds(130, 122, 200, 20);
	studentPanel.add(FatherNameField);

	EmailField = new JTextField();
	EmailField.setEditable(false);
	EmailField.setForeground(new Color(47, 79, 79));
        EmailField.setBackground(new Color(210, 210, 210));
	EmailField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	EmailField.setColumns(10);
	EmailField.setBounds(130, 162, 200, 20);
	studentPanel.add(EmailField);

	FineField = new JTextField();
	FineField.setEditable(false);
	FineField.setForeground(new Color(47, 79, 79));
        FineField.setBackground(new Color(210, 210, 210));
	FineField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
	FineField.setColumns(10);
	FineField.setBounds(130, 202, 200, 20);
	studentPanel.add(FineField);
        
        JLabel DateLabel = new JLabel(" Date of Issue");
	DateLabel.setForeground(new Color(255, 69, 0));
	DateLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
	DateLabel.setBounds(330, 320, 118, 26);
	add(DateLabel);

        IssuedDateField = new JTextField();
        IssuedDateField.setEditable(false);
	IssuedDateField.setForeground(new Color(47, 79, 79));
        IssuedDateField.setBackground(new Color(210, 210, 210));
	IssuedDateField.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
        IssuedDateField.setText(CurrentDate());
	IssuedDateField.setColumns(10);
	IssuedDateField.setBounds(450, 320, 200, 25);
	add(IssuedDateField);
        

        IssueBtn = new JButton("Issue");
	IssueBtn.addActionListener(this);
	IssueBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
	IssueBtn.setBorder(new LineBorder(new Color(192, 192, 192), 1, true));
	IssueBtn.setBounds(380, 380, 115, 33);
	IssueBtn.setBackground(Color.BLACK);
        IssueBtn.setForeground(Color.WHITE);
        add(IssueBtn);

	BackBtn = new JButton("Back");
	BackBtn.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        BackBtn.addActionListener(this);
        BackBtn.setBackground(Color.BLACK);
        BackBtn.setForeground(Color.WHITE);
	BackBtn.setBorder(new LineBorder(new Color(192, 192, 192), 1, true));
	BackBtn.setBounds(510, 380, 115, 33);
        add(BackBtn);




        
        
        
        
        
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
}
    
    public static void main(String[] args) {
       new IssueBook().setVisible(true);
    }
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(e.getSource() == SearchBookBtn) {
            try {
                conn con = new conn();
                String sql = "select * from books where Book_Id = ?";
		PreparedStatement st = con.c.prepareStatement(sql);
		st.setString(1, BookIdField.getText());
		ResultSet rs = st.executeQuery();
		
                while (rs.next()) {
                    BookTitleField.setText(rs.getString("Book_Title"));
                    BookAuthorField.setText(rs.getString("Book_Author"));
                    PublisherField.setText(rs.getString("Publisher"));
                    EditionField.setText(rs.getString("Edition"));
                    PriceField.setText(rs.getString("Price"));
                    PagesField.setText(rs.getString("Total_Pages"));
                    AvailableField.setText(rs.getString("Total_Stock"));
                    //IssuedDateField.setText(CurrentDate());
		}
		st.close();
		rs.close();
            } catch(Exception e2) {
                e2.printStackTrace();
            }
        }
        if(e.getSource() == SearchStudentBtn) {
            try {
                conn con = new conn();
                String sql = "select * from user where Id = ?";
		PreparedStatement st = con.c.prepareStatement(sql);
		st.setString(1, StudentIdField.getText());
		ResultSet rs = st.executeQuery();
		
                while (rs.next()) {
                    NameField.setText(rs.getString("Name"));
                    FatherNameField.setText(rs.getString("Father_Name"));
                    EmailField.setText(rs.getString("Email"));
                    FineField.setText(rs.getString("Fine"));
		}
		st.close();
		rs.close();
            } catch(Exception e2) {
                e2.printStackTrace();
            }
            
        }
        if(e.getSource() == BackBtn) {
            setVisible(false);
        }
        if(e.getSource() == IssueBtn) {
                try{
                    if(AvailableField.getText() != "0") {                    
                    conn con = new conn();
                    String sql = "INSERT into issued_books(Student_Id, Book_Id, Issue_Date, Return_Date) values(?, ?, ?, ?)";
                    PreparedStatement st = con.c.prepareStatement(sql);
                    st.setString(1, StudentIdField.getText());
                    st.setString(2, BookIdField.getText());
                    
                    
                    DateFormat df=new SimpleDateFormat("MMM dd, yyyy");
//                    String ADate=df.format(dateChooser.getDate());
//                    Date date =  df.parse(ADate);  //Error comes here
                    Calendar cal = Calendar.getInstance();      
                    cal.add(Calendar.DAY_OF_MONTH, 7);
                    Date futureDate = cal.getTime();
                    String outD=df.format(futureDate); //And also here
                    System.out.println(outD);
                    
                    
                    
                    st.setString(3,CurrentDate());
                    st.setString(4, outD);
                    int i = st.executeUpdate();
                    if (i > 0) {
                        JOptionPane.showMessageDialog(null, "Book Issued Successfully..!");
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Out of Stock...!.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    }
                    else {
                         JOptionPane.showMessageDialog(null, "error");
                    }
                    } catch(Exception ae){
                                }
        }
    }
    
}
