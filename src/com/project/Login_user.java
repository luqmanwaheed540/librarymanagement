package com.project;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.ImageIcon;



class BackgroundPanel extends Panel
{
    // The Image to store the background image in.
    Image img;
    public BackgroundPanel()
    {
        // Loads the background image and stores in img object.
        img = Toolkit.getDefaultToolkit().createImage("bg.jpg");
    }

    public void paint(Graphics g)
    {
        // Draws the img to the BackgroundPanel.
        g.drawImage(img, 0, 0, null);
    }
}

public class Login_user extends JFrame implements ActionListener{

	private JPanel panel;
	private JTextField sapID;
	private JPasswordField password;
        private JButton login, forget;
        private ImageIcon resizeImage;
        //Image img = Toolkit.getDefaultToolkit().createImage("bg.png");
        
        
	public Login_user() {
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel = new JPanel();
	panel.setBackground(new Color(176, 224, 230));
	setContentPane(panel);
	panel.setLayout(null);
        
        
        JLabel heading = new JLabel(" Login Form");
        heading.setFont(new Font("Aril", Font.BOLD, 50));
        //heading.setOpaque(true);
        heading.setForeground(Color.WHITE);
        //heading.setBackground(new Color(176, 176, 176));
	heading.setBounds(650, 100, 300, 80);
	panel.add(heading);
        
	JLabel IdLabel = new JLabel("Sap ID : ");
        IdLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        IdLabel.setForeground(Color.WHITE);
	IdLabel.setBounds(590, 300, 100, 24);
	panel.add(IdLabel);

	JLabel PassLabel = new JLabel("Password : ");
        PassLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        PassLabel.setForeground(Color.WHITE);
	PassLabel.setBounds(565, 350, 120, 24);
	panel.add(PassLabel);

	sapID = new JTextField();
	sapID.setBounds(680, 295, 250, 30);
        sapID.setFont(new Font("Arial", Font.PLAIN, 15));
	panel.add(sapID);
	
	password= new JPasswordField();
	password.setBounds(680, 345, 250, 30);
        password.setFont(new Font("Arial", Font.PLAIN, 15));
	panel.add(password);

	login = new JButton("Login");
        login.setBorder(null);
	login.addActionListener(this);   
	login.setForeground(Color.WHITE);
	login.setBackground(new Color(74, 252, 73));
        login.setFont(new Font("Arial", Font.BOLD + Font.ITALIC, 20));
	login.setBounds(670, 400, 200, 50);
	panel.add(login);
        
        
	forget = new JButton("Forgot Password?");
        forget.setBorder(null);
	forget.addActionListener(this);
        forget.setForeground(Color.WHITE);
	forget.setBackground(new Color(255, 1, 1));
        forget.setFont(new Font("Arial", Font.BOLD + Font.ITALIC, 20));
	forget.setBounds(670, 470, 200, 50);
	panel.add(forget);
        
        
        
        ImageIcon img = new ImageIcon("bg.png");
        JLabel bgLabel = new JLabel("", img, JLabel.CENTER);
        bgLabel.setBounds(0, 0, 1700, 1000);
        add(bgLabel);
        
	}
        
        public void actionPerformed(ActionEvent ae){
            if(ae.getSource() == login){
                Boolean status = false;
		try {
                    conn con = new conn();
                    String sql = "SELECT * FROM user WHERE Id=? AND Password=?";
                    PreparedStatement st = con.c.prepareStatement(sql);

                    st.setString(1, sapID.getText());
                    st.setString(2, password.getText());

                    ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        if("0".equals(rs.getString("Role"))) {
                            new AdminHome().setVisible(true);
                        }
                        else if ("1".equals(rs.getString("Role"))) {
                            
                        }       
                    } else {
			JOptionPane.showMessageDialog(null, "Invalid Login...!.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                       
		} catch (Exception e2) {
                    e2.printStackTrace();
		}
            }  
            if(ae.getSource() == forget){
                setVisible(true);
               new ForgetPassword().setVisible(true);
		//Forgot forgot = new Forgot();
		//forgot.setVisible(true);
            }
        }
        
  	public static void main(String[] args) {
                new Login_user().setVisible(true);
	}

}
